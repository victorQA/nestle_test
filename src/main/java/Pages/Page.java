package Pages;

import drivers.Browser;
import drivers.World;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.TimeOuts;

/**
 * Created by Victor on 20/07/17.
 * General functions for every page
 */
public class Page {

    private Browser browser;
    protected WebDriver driver;
    private String valueBasket;

    //constructor
    public Page() {
        this.browser = World.getInstance().getBrowser();
        this.driver = browser.getDriver();
    }

    /**
     * Getters
     *
     * @return the value basket
     */
    public String getValueBasket() {
        return valueBasket;
    }

    /**
     * Setters
     * set value basket
     * @param valueBasket: current value in basket
     */
    public void setValueBasket(String valueBasket) {
        this.valueBasket = valueBasket;
    }


    /**
     * Created by Victor on 20/07/17.
     * wait instance
     * @return the WebDriverWait instance (driver, 20)
     */
    public WebDriverWait waitInstance(){
        return new WebDriverWait(driver, TimeOuts.FIND_ELEMS_INSECS);
    }

}
