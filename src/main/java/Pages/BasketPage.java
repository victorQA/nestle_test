package Pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Victor on 19/07/2017.
 * Basket page
 */
public class BasketPage extends Page {
    WebElement checkoutButton = driver.findElement(By.id("hlb-ptc-btn"));

    //constructor
    public BasketPage() {

    }

    /**
     * verify if the product is added in the basket
     */
    public void checkProduct() {
        boolean addedProduct = false;
        if (getValueBasket() != driver.findElement(By.id("nav-cart-count")).getText()){
          addedProduct = true;
        }
        Assert.assertTrue("FAILED: The product was not added", addedProduct);
    }

    /**
     * Click on process checkout button
     */
    public void clickOnCheckout(){
        waitInstance().until(ExpectedConditions.elementToBeClickable(checkoutButton));
        checkoutButton.click();
    }
}
