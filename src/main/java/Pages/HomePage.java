package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Victor on 19/07/2017.
 * Amazon home page
 */
public class HomePage extends Page {
    // elements' Amazon home page
    WebElement formSearch = driver.findElement(By.name("site-search"));
    WebElement searchBar = formSearch.findElement(By.name("field-keywords"));
    WebElement searchButton = formSearch.findElement(By.cssSelector("input[class='nav-input']"));

    //constructor
    public HomePage() {

    }

    /**
     * Verify if Amazon home is loaded properly
     */
    public void websiteIsLoad(){
        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.id("nav-logo")));
    }

    /**
     * find product from search bar
     * @param product
     */
    public void searchProduct (String product) {
        boolean findProduct = false;
        searchBar.sendKeys(product);
        searchButton.click();

        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.id("searchSortForm")));

        //Verify if the product was found
        String expectedSearch = driver.findElement(By.id("s-result-info-bar-content")).getText();
        if (expectedSearch.toLowerCase().contains(product.toLowerCase())) {
            findProduct = true;
        }
        Assert.assertTrue("FAILED: The product was not searched", findProduct);
    }

}
