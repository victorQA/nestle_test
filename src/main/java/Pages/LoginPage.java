package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Victor on 19/07/2017.
 * Login Page
 */
public class LoginPage extends Page {
    //constructor
    public LoginPage() {

    }

    /**
     * Verify if login page is loaded properly
     */
    public void loginPageIsLoad(){
        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.name("signIn")));
    }
}
