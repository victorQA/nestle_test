package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

/**
 * Created by Victor on 19/07/2017.
 * Product page
 */
public class ProductPage extends Page {
    // Element of product page
    WebElement searchSortForm = driver.findElement(By.id("searchSortForm"));

    //constructor
    public ProductPage() {

    }

    /**
     * Order product list by Asc price
     */
    public void orderProductByAscPrice(){
        Select select = new Select(searchSortForm.findElement(By.id("sort")));
        select.selectByValue("price-asc-rank");
        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.id("s-results-list-atf")));
    }

    /**
     * Select first element in product list
     */
    public void selectFirstProduct(){
        WebElement searchResult = driver.findElement(By.id("s-results-list-atf"));
        List<WebElement> ListProduct= searchResult.findElements(By.tagName("li"));
        ListProduct.get(0).findElement(By.tagName("img")).click();
        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.className("a-box")));

    }

    /**
     * Add a product in basket
     */
    public void addProduct(){
        //check the current value of basket and set it
        WebElement Basket = driver.findElement(By.id("nav-cart-count"));
        String currentValue = Basket.getText();
        setValueBasket(currentValue);

        // add product in basket
        WebElement addBasketButton = driver.findElement(By.id("add-to-cart-button"));
        addBasketButton.click();

        //wait until basket page is loaded
        waitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.id("huc-v2-order-row-container")));

    }

}
