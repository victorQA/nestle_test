package utils;

/**
 * Created by Victor on 20/07/17.
 */
public final class TimeOuts {

    // Class variables
    public static final int FIND_ELEMS_INSECS = 20;

    // Class Constructor
    private TimeOuts() {
    }
}