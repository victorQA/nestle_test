package drivers;


/**
 * Created by Victor on 21/07/17.
 * Create unique browser instance
 */
public class World {

    private static World instance = new World();
    private static Browser browser = new Browser();

    private World(){}

    public static World getInstance() {
        return instance;
    }

    public Browser getBrowser() {
        return browser;
    }

}
