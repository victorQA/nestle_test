package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.net.MalformedURLException;

/**
 * Created by Victor on 20/07/17.
 * get browser and driver
 */
public class Browser {
    private static WebDriver driver = null;

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver getBrowser(String browserType) {
        if (driver == null) {
            if("firefox".equals(browserType)) {
                driver = new FirefoxDriver();
            } else if ("chromeLinux".equals(browserType)){
                System.setProperty("webdriver.chrome.driver", "chromedriverLinux");
                driver = new ChromeDriver();
            } else if ("chromeWin32".equals(browserType)){
                System.setProperty("webdriver.chrome.driver", "chromedriverwin32.exe");
                driver = new ChromeDriver();
            } else {
                throw new IllegalArgumentException("the " + browserType + " does not exist");
            }
            // Maximize Browser size
            driver.manage().window().maximize();
        }
        return driver;
    }
}
