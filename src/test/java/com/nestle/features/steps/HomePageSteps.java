package com.nestle.features.steps;

import Pages.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/**
 * Created by Victor on 19/07/2017.
 */
public class HomePageSteps {

    private HomePage homePage = new HomePage();
    @Given("^Amazon website is loaded$")
    public void websiteIsLoad(){
        homePage.websiteIsLoad();
    }

    @When("^the search ([^\"]*) is entered$")
    public void searchProduct(String product) {
        homePage.searchProduct(product);
    }

}
