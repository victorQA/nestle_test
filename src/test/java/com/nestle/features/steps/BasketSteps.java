package com.nestle.features.steps;

import Pages.BasketPage;
import cucumber.api.java.en.Then;

/**
 * Created by Victor on 19/07/2017.
 */
public class BasketSteps {
    BasketPage basketPage = new BasketPage();

    @Then("^the basket has the product selected$")
    public void basketIsNotEmpty(){
        basketPage.checkProduct();
    }
}
