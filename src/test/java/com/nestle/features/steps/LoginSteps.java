package com.nestle.features.steps;

import Pages.BasketPage;
import Pages.LoginPage;
import cucumber.api.java.en.Then;

/**
 * Created by Victor on 19/07/2017.
 */
public class LoginSteps {
    LoginPage login = new LoginPage();
    BasketPage basket = new BasketPage();

    @Then("^the purchase process can start$")
    public void loginPageIsLoaded() {
        basket.clickOnCheckout();
        login.loginPageIsLoad();
    }

}
