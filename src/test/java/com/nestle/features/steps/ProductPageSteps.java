package com.nestle.features.steps;

import Pages.ProductPage;
import cucumber.api.java.en.When;

/**
 * Created by Victor on 19/07/2017.
 */
public class ProductPageSteps {
    private ProductPage productPage = new ProductPage();

    @When("^the product list is ordered by asc price$")
    public void orderProductListByAscPrice(){
        productPage.orderProductByAscPrice();
    }

    @When("^the first cheaper product is selected$")
    public void selectCheapProduct() {

        productPage.selectFirstProduct();

    }
    @When("^the product is added to the basket$")
    public void addProduct(){
        productPage.addProduct();
    }
}
