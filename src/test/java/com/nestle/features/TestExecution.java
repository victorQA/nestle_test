package com.nestle.features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import drivers.Browser;
import drivers.World;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;


/**
 * Created by Victor on 20/07/17.
 */

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-json-report.json"},
        features = "src/test/resources/features/")

public class TestExecution {
    private static Browser browser = World.getInstance().getBrowser();
    private static WebDriver driver = browser.getDriver();
    private static String selectedDriver;
    private static String baseUrl;


    @BeforeClass
    public static void initialize() throws MalformedURLException {
        selectedDriver = System.getProperty("browser");
        baseUrl = System.getProperty("baseUrl");
        driver = browser.getBrowser(selectedDriver);
        driver.get(baseUrl);
    }

    @AfterClass
    public static void end(){
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
