*****************
Pre-requisites:
*****************
- Java SDK 1.8.0_131 installed

- Automation project can be launched on Windows or Linux.
"Pom.xml" file can be updated depening on your browser (By defaul: chromeWin32).
property: browser
values: chromeWin32 or chromeLinux or firefox.

- If chromedriverLinux file does not have permissions, open the terminal and execute the command: chmod +x FILE

************************
possible improvements:
************************
We can improve the steps, for examples,
1- "And the product list is ordered by asc price".
Currently "asc price" is not a paramenter but the step could have a parameter and depend on it, the list can be ordered by the parameter.
2- "And the first cheaper product is selected"
Currently the step launches a specific method to select the first cheaper product but it could be reviewed and create a generic step to select a specific product
3- The steps can be review and improve the sentences of every step.
