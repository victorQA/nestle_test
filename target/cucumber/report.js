$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Amazon.feature");
formatter.feature({
  "line": 1,
  "name": "Buy the most cheaper product in Amazon",
  "description": "",
  "id": "buy-the-most-cheaper-product-in-amazon",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Buy the most cheaper product in Amazon",
  "description": "",
  "id": "buy-the-most-cheaper-product-in-amazon;buy-the-most-cheaper-product-in-amazon",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Amazon website is loaded",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the search portatil i5 is entered",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "the product list is ordered by asc price",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "the first cheaper product is selected",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "the product is added to the basket",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "the basket has the product selected",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "the purchase process can start",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.websiteIsLoad()"
});
formatter.result({
  "duration": 2436769648,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "portatil i5",
      "offset": 11
    }
  ],
  "location": "HomePageSteps.searchProduct(String)"
});
formatter.result({
  "duration": 7457523652,
  "status": "passed"
});
formatter.match({
  "location": "ProductPageSteps.orderProductListByAscPrice()"
});
formatter.result({
  "duration": 2705028584,
  "status": "passed"
});
formatter.match({
  "location": "ProductPageSteps.selectCheapProduct()"
});
formatter.result({
  "duration": 5835027897,
  "status": "passed"
});
formatter.match({
  "location": "ProductPageSteps.addProduct()"
});
formatter.result({
  "duration": 3190822431,
  "status": "passed"
});
formatter.match({
  "location": "BasketSteps.basketIsNotEmpty()"
});
formatter.result({
  "duration": 191886303,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.loginPageIsLoaded()"
});
formatter.result({
  "duration": 2603755017,
  "status": "passed"
});
});