Feature: Buy the most cheaper product in Amazon

  Scenario: Buy the most cheaper product in Amazon
    Given Amazon website is loaded
    When the search portatil i5 is entered
    And the product list is ordered by asc price
    And the first cheaper product is selected
    And the product is added to the basket
    Then the basket has the product selected
    And the purchase process can start
